Project: Hello World
====================

Language: Java
--------------

Context: Coursera Princeton Algorithms Part II Course
-----------------------------------------------------

Abstract:
---------

This is the Hello World program used to test Java environment configuration
for the Coursera Princeton Algorithms Part II Course.
